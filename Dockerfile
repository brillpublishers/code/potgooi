FROM python:3.7.4-alpine
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY shortcodes.py /usr/local/bin/
COPY potgooi /usr/local/bin
WORKDIR /blogs
CMD ["/usr/local/bin/potgooi"]
