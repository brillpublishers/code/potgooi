# Potgooi

Utility scripts for generating and maintaining blogs and documentation. Takes Markdown files plus data as input and generates HTML output.

## Usage

First a Docker image file needs to be built using:
`docker build -t potgooi .`

Then you can run the built docker image in a working directory containing the data files like:

`docker run -ti -v $(pwd):/blogs potgooi`

When running the gen command, the following sub-directories are expected in the current directory:

- posts
- templates
- authors

For each Markdown file `<filename>.md` in the ./posts/ path, generates a corresponding output `<filename>.html` file.

The generated HTML can be found in ./out/

## TODO

Add Server support using the http.server module and .exe preview support for Windows users

## Done

Add https://getnikola.com/handbook.html#shortcodes support, so that we can for example do:

`{{% media /media/abc.mp3 %}}`

Which will then be turned into:

```
<audio id="abc" crossorigin="" playsinline="">
    <source src="/media/abc.mp3" type="audio/mp3">
</audio>
<script>
    new Plyr('#abc', {
        iconUrl: '/assets/plyr/plyr.svg'
    });
</script>
```
